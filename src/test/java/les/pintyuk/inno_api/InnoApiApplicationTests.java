package les.pintyuk.inno_api;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Type;
import java.util.HashMap;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class InnoApiApplicationTests {

    private Integer port;

    @Test
    void multi() {

        RequestSpecification request = RestAssured.given();
        RestAssured.baseURI = "http://localhost" + port;
        Response response = request.get("/multi/4");

        Assertions.assertEquals(200, response.getStatusCode());
        Assertions.assertEquals(4, response.jsonPath().getInt("1"));
        Assertions.assertEquals(16, response.jsonPath().getInt("2"));
        Assertions.assertEquals(64, response.jsonPath().getInt("3"));
        Assertions.assertEquals(256, response.jsonPath().getInt("4"));

    }

}
