package les.pintyuk.endpoints;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class Multi {

    @GetMapping(path = "/multi/{i}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String multi(@PathVariable Integer i) {
        JSONObject j = new JSONObject();
        for (Integer k = 1; k < 5; k++) {
            j.put(k.toString(), Math.pow(i, k));
        }
        //System.out.println(j);
        return j.toString();
    }
}
