package les.pintyuk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class  InnoApiApplication {

    public static void main(String[] args) {

        SpringApplication.run(InnoApiApplication.class, args);
    }

}
